import os
from urllib.request import urlretrieve
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


import plotly
import plotly.express as px

import base64
from io import BytesIO


filepath = "hearth-attack-prediction.csv"

if not os.path.isfile(filepath):
    url = "https://gitlab.com/xtec/bio-data/-/raw/main/heart-attack-prediction.csv"
    urlretrieve(url, filepath)

df = pd.read_csv(filepath)
print(df.iloc[0])


#top 10 people with the highest cholesterol levels
df_cholesterol = df.sort_values(by='Cholesterol', ascending=False)
top10_Cholesterol = df_cholesterol[["Age","Sex","Country"]].head(10)
print(top10_Cholesterol)


#Average physical activity - top 10 countries 
average_activity_by_country = round(df.groupby('Country')['Physical Activity Days Per Week'].mean(),2)
average_activity_by_country_df = average_activity_by_country.reset_index()
print(average_activity_by_country_df)


def __draw_plot():
    '''Transforma el gràfic en una imatge.'''
    img = BytesIO()
    plt.savefig(img, format='jpg')
    plt.close()
    img.seek(0)
    plot_url = base64.b64encode(img.getvalue()).decode('utf8')
    return plot_url


def crear_plot1(df):
#Dependence of cholesterol levels on stress levels
    sns.barplot(x='Stress Level', y='Cholesterol', data=df)
    plt.savefig('images\graph_1.png')
    #plt.show()
    plot_result1 = __draw_plot()
    return plot_result1
    # plt.close()

def crear_plot2(df):
# Cholesterol Level per Continent
    sns.set(style="whitegrid")
    plt.figure(figsize=(12, 6))
    sns.barplot(data=df, x="Cholesterol", y="Continent", hue="Sex")
    plt.title("Cholesterol Level per Continent")
    plt.xlabel("Cholesterol")
    plt.ylabel("Continent")
    plot_result2 = __draw_plot()
    # plt.close()
    return plot_result2



# Cholesterol Level per Continent

""" sns.set(style="whitegrid")
plt.figure(figsize=(12, 6))
sns.barplot(data=df, x="Cholesterol", y="Continent", hue="Sex")
plt.title("Cholesterol Level per Continent")
plt.xlabel("Cholesterol")
plt.ylabel("Continent")
plt.savefig('images\graph_2.png')
plt.show()
 """

#map

def crear_mapa(df):

    patients_by_country = df.groupby('Country').size().reset_index(name='PatientsCount')


    df = pd.merge(df, patients_by_country, on='Country', how='left')


    """ fig = px.choropleth(df,
                        locations="Country",
                        locationmode="country names",
                        color='PatientsCount',
                        color_continuous_scale="Viridis",
                        title="Number of Patients by Country")


    fig.show() """

    fig = px.scatter_geo(df, 
                        locations="Country", 
                        locationmode="country names",
                        color="PatientsCount", 
                        hover_name="Country",
                        size="PatientsCount",
                        projection="natural earth",
                        title="Number of Patients by Country")


    fig.show()
    fig.write_image('images/mapa.png')
    mapa = __draw_plot()
    # plt.close()
    return mapa

#crear_mapa(df)