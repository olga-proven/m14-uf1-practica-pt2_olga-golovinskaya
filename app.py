from flask import Flask, render_template
from data import *

from flask import Flask, render_template, request, Response
from flask_restful import Resource, Api

import json



app = Flask(__name__)
api = Api(app)

""" 
class CustomResource(Resource):

    def get(self, country):
        data = df[df['Country'] == country]
        if data.empty:
            return {"error": f"No patients found for the country {country}"}, 404

        return Response(
            data.to_json(orient="records"),
            mimetype='application/json')

api.add_resource(CustomResource, "/patients/<country>")
 """

@app.route("/country")
def patients_by_country():
    country = request.args.get('country')   
    if country is None:
        return {"error": "country parameter is missing"}, 400

    data = df[df['Country'] == country]
    if data.empty:
        return {"error": f"No patients found for the country {country}"}, 404

    return Response(
        data.to_json(orient="records"),
        mimetype='application/json')


@app.route("/patients", methods=['GET'])
def patients():
    df_cholesterol = df.sort_values(by='Cholesterol', ascending=False)
    top10_Cholesterol = df_cholesterol[["Age", "Sex", "Country"]].head(10)
    return Response(
        top10_Cholesterol.to_json(orient="records"),
        mimetype='application/json')


@app.route("/activity", methods=['GET'])
def activity():
    average_activity_by_country = round(df.groupby('Country')['Physical Activity Days Per Week'].mean(),2)
    average_activity_by_country_df = average_activity_by_country.reset_index()
    return Response(
        average_activity_by_country_df.to_json(orient="records"),
        mimetype='application/json')


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/apis")
def apis():
    return render_template("apis.html")

@app.route("/plots")
def plots():
    #return render_template("plots.html")
    plot_result1 = crear_plot1(df)
    plot_result2 = crear_plot2(df)
    return render_template("plots.html",grafic1=plot_result1, grafic2=plot_result2)

@app.route("/maps")
def maps():
    result=crear_mapa(df)
    return render_template("maps.html", mapa = result)

if __name__== "__main__":
    app.run(debug=True)